import gitlab
import os
import sys
import json
import logging
from merge_request_diff import MergeRequestDiff

logging.basicConfig(level=logging.INFO)
GITLAB_SERVER = os.environ.get("GITLAB_SERVER", "https://gitlab.com")
GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN")
GITLAB_PROJECT_ID = "5261717"  # VS code project
BUG_LABEL = "type::bug"


if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

project = gl.projects.get(id=GITLAB_PROJECT_ID)


def run_bug_file_generator():
    merged_bug_mrs = []

    logging.info("getting all merged bug merge requests")
    for mr in project.mergerequests.list(get_all=False):
        if mr.state == "opened":
            pass
        if mr.state == "merged":
            if BUG_LABEL in mr.labels:
                logging.info(f"found merged bug MR: {mr.title}")
                merged_bug_mrs.append(mr)

    logging.info(f"found {len(merged_bug_mrs)} merged bug merge requests")

    # Create Merge Request Diff objects from the merges
    logging.info("creating MergeRequestDiff objects")
    merge_request_diffs: list[MergeRequestDiff] = []
    merge_request_files: list[tuple[str, str]] = []

    for mr in merged_bug_mrs:
        mr_obj = MergeRequestDiff(
            project_id=project.id,
            merge_request=mr,
            mr_commit=project.commits.get(id=mr.merge_commit_sha),
            diffs=project.commits.get(id=mr.merge_commit_sha, get_all=True).diff(
                get_all=True
            ),
        )
        merge_request_diffs.append(mr_obj)
        merge_request_files.append(mr_obj.get_diff_files())

    """
    # create a dictionary of files and the line nubmers that were fixed
    # will look like this:
    {
        "file1": ['192,7', '109,9'],
    }
    """
    bug_files_fixed: dict[str, list] = {}
    for merge_file_list in merge_request_files:
        for merge_file in merge_file_list:
            # print(merge_file[0])
            # print(merge_file[1])
            if bug_files_fixed.get(merge_file[0]):
                bug_files_fixed[merge_file[0]].append(merge_file[1])
            else:
                bug_files_fixed[merge_file[0]] = [f"{merge_file[1]}"]

    logging.info("writing json to disk")
    with open("bug_files_fixed.json", "w") as f:
        f.write(json.dumps(bug_files_fixed, indent=4))


if __name__ == "__main__":
    run_bug_file_generator()
