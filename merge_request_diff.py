class MergeRequestDiff:
    def __init__(
        self, project_id: str, merge_request: dict, diffs: list[dict], mr_commit: dict
    ):
        self.project_id = project_id
        self.merge_request_id = merge_request.id
        self.merge_request = merge_request
        self.diffs = diffs
        self.mr_commit = mr_commit

    def __str__(self) -> str:
        return f"project id: {self.project_id} | merge request id: {self.merge_request_id} | mr commit stats: {self.mr_commit.stats}"

    def get_diff_files(self) -> list[tuple[str, str]]:
        # will return a tuple of (filename, diff new file hunk range)
        return [
            (diff["new_path"], diff["diff"].split("@@")[1].split("+")[1].strip())
            for diff in self.diffs
        ]
